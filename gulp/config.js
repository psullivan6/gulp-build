require('dotenv').config();
const packageConfig = require('../package.json');

const srcDir = 'assets';
const publicDir  = (process.env.CACHE_BUST != null) ?
  `public/${process.env.CACHE_BUST}` :
  'public';


module.exports = {
  projectSettings: {
    name: packageConfig.name,
    notificationTemplate(taskName) {
      return {
        title   : this.name,
        message : `${taskName} compiled`,
        onLast  : true,
        // icon: this.icon,
      };
    },
    srcDir,
    publicDir
  },
  images: {
    enabled        : true, // flag for use with the createTask-based build system
    src            : `${srcDir}/img/**/*.{jpg,jpeg,png,svg,gif}`,
    dest           : `${publicDir}/img`,
    minify         : true,
    minifySettings : {
      svgoPlugins: [
        { removeAttrs: false },
        { removeUselessDefs: false },
      ],
    },
  },
  scripts: {
    enabled : true, // flag for use with the createTask-based build system
    src     : [
      `${srcDir}/js/*.js`,
      `${srcDir}/js/pages/*.js`,
    ],
    base: `${srcDir}/js`,

    // This should match the libs config below and visa-versa
    externalLibraries: {
      jquery      : 'window.jQuery',
      lodash      : 'window._',
      react       : 'window.React',
      'react-dom' : 'window.ReactDOM',
    },
    dest           : `${publicDir}/js`,
    minify         : true,
    rollupSettings : {
      format: 'iife',
    },
  },
  styles: {
    enabled : true, // flag for use with the createTask-based build system
    src     : [
      `${srcDir}/scss/**/*.scss`,
      `!${srcDir}/scss/**/_*.scss`,
    ],
    base   : `${srcDir}/scss`,
    dest   : `${publicDir}/css`,
    minify : true,
  },
  libs: {
    enabled : true, // flag for use with the createTask-based build system
    src     : [
      'node_modules/react/umd/react.development.js', // [TODO] Change this to production or make it ENV-based
      'node_modules/react-dom/umd/react-dom.development.js', // [TODO] Change this to production or make it ENV-based
    ],
    dest   : `${publicDir}/libs`,
    minify : false,
  }
};
