const path = require('path');
const gulp = require('gulp');
const notify = require('gulp-notify');
const rename = require('gulp-rename');
const intelliWatch = require('gulp-intelli-watch');
const logError = require('./log-error');

const config = require('../config');

const destFolder = dest =>
  path.extname(dest).length ? path.parse(dest).dir : dest;

/**
 * Task Generator
 * Pull settings from the config file found at `./gulp_tasks/config.js`. The taskName provided is the key to the config
 * settings to use for this task. A watch task will also be created if there is a watch string provided in the config.
 */
module.exports = function({
  taskName,
  buildProcess = src => src,
  minifier = false,
  minifySuffix = '-min'
}) {
  // generate task name for the watcher
  const watchTaskName = `${taskName}:watch`;

  // get the settings specific to this task
  const taskConfig = config[taskName];

  function taskLogic(src) {
    const options = {};

    if (taskConfig.base) {
      options.base = taskConfig.base;
    }

    // create gulp source
    let gulpSrc = gulp.src(src, options)
      // log errors for all tasks
      .on('error', logError);

    gulpSrc = buildProcess(gulpSrc);

    // don't waste time writing files if minified files will overwrite them
    if (!(taskConfig.minify && minifier && !minifySuffix)) {
      gulpSrc = gulpSrc
        .pipe(gulp.dest(destFolder(taskConfig.dest)));
    }

    gulpSrc = gulpSrc.pipe(notify(
      config.projectSettings.notificationTemplate(taskName)
    ));

    if (taskConfig.minify && minifier) {
      gulpSrc = gulpSrc
        .pipe(rename({
          suffix: minifySuffix
        }))
        .pipe(minifier(taskConfig.minifySettings || {}))
        .pipe(gulp.dest(destFolder(taskConfig.dest)));
    }

    return gulpSrc;
  }

  // this is to get the function name the same as the task
  const t = { [taskName]: () => taskLogic(taskConfig.src) };
  const watcher = intelliWatch(taskConfig.src, taskLogic);

  Object.assign(t, {
    [watchTaskName]: () => watcher(),
  });

  gulp.task(taskName, t[taskName]);

  gulp.task(watchTaskName, t[watchTaskName]);

  // return the names of the build task and watch task
  return { task: t[taskName], watchTask: t[watchTaskName] };
};
