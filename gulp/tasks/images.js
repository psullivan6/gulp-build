// Libs / Helpers
const minifier = require('gulp-imagemin');
const newer    = require('gulp-newer');

// Relative Libs / Helpers
const { images: config } = require('../config');
const createTask = require('../utils/task-generator');



// [TODO] REMOVE files at destination that are not present in source



module.exports = createTask({
  taskName: 'images',
  buildProcess(gulpSrc) {
    return gulpSrc
      .pipe(newer(config.dest));
  },
  minifySuffix: '',
  minifier
});
