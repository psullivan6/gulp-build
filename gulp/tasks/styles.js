// Libs / Helpers
const autoprefixer = require('gulp-autoprefixer');
const importer     = require('node-sass-magic-importer')(); // call the function as the docs require
const minifier     = require('gulp-cssnano');
const sass         = require('gulp-sass');
const sourcemaps   = require('gulp-sourcemaps');

// Relative Libs / Helpers
const createTask   = require('../utils/task-generator');
const logError     = require('../utils/log-error');


module.exports = createTask({
  taskName: 'styles',
  buildProcess(gulpSrc) {
    return gulpSrc
      .pipe(sourcemaps.init())
      .pipe(sass({ importer }).on('error', logError))
      .pipe(autoprefixer());
  },
  minifier,
});
