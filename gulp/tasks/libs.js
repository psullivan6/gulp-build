// Relative Libs / Helpers
const createTask = require('../utils/task-generator');

// Super simply "move" task, so just return the source and carry-on
module.exports = createTask({
  taskName: 'libs',
  buildProcess(gulpSrc) {
    return gulpSrc;
  },
});
