const del            = require('del');
const parseGitignore = require('parse-gitignore');


module.exports = function(callback) {
  // Parse the .gitignore file and ensure node_modules remain intact
  const gitignorePaths = parseGitignore('.gitignore', ['!node_modules/**']);

  return del(gitignorePaths, callback);
};
