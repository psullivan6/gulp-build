// Libs / Helpers
const buble        = require('rollup-plugin-buble');
const buffer       = require('vinyl-buffer');
const commonjs     = require('rollup-plugin-commonjs');
const flatmap      = require('gulp-flatmap');
const minifier     = require('gulp-uglify');
const nodeResolve  = require('rollup-plugin-node-resolve');
const path         = require('path');
const replace      = require('rollup-plugin-replace');
const source       = require('vinyl-source-stream');
const sourcemaps   = require('gulp-sourcemaps');
const { Readable } = require('stream');
const { rollup }   = require('rollup');

// Relative Libs / Helpers
const { scripts: config } = require('../config');
const createTask          = require('../utils/task-generator');
const logError            = require('../utils/log-error');

// Variables
const globals = config.externalLibraries;
const external = Object.keys(globals);
const settingsCache = {};
const environment = (process.env.NODE_ENV != null) ? process.env.NODE_ENV : 'production';

Object.assign(config.rollupSettings, {
  sourcemap : true,
  plugins   : [
    nodeResolve({
      extensions: ['.js', '.jsx'],
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify(environment)
    }),
    commonjs(),
    buble(),
  ],
  external,
  globals,
});


module.exports = createTask({
  taskName: 'scripts',
  buildProcess(gulpSrc) {
    return gulpSrc
      .pipe(flatmap((s, file) => {
        const stream = new Readable();
        stream._read = function() {};

        if (!settingsCache[file.path]) {
          settingsCache[file.path] = Object.assign({}, config.rollupSettings, {
            input : file.path,
            file  : file.path,
          });
        }

        rollup(settingsCache[file.path])
          .then((bundle) => {
            bundle.generate(settingsCache[file.path]).then(({ code, map }) => {
              settingsCache[file.path].cache = bundle;

              stream.push(code);

              if (settingsCache[file.path].sourcemap) {
                stream.push(`\n//# sourceMappingURL=${map.toUrl()}`);
              }

              stream.push(null);
            });
          })
          .catch(logError);

        return stream
          .pipe(source(path.relative('assets/js', file.path), 'assets/js'))
          .pipe(buffer())
          .pipe(sourcemaps.init({ loadMaps: true }));
      }));
  },
  minifier,
});
