// Libs / Helpers
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';


export default class CoolThing extends Component {
  constructor(props) {
    super(props);

    console.log('PROPS', props);
  }

  render() {
    return (
      <Fragment>
        <header>
          <h1>Some Cool React Stuff</h1>
          <p>Here are the props:</p>
        </header>
        <ul>
          { Object.keys(this.props).map(item => <li key={ item.toString() }>{ this.props[item].toString() }</li>) }
        </ul>
      </Fragment>
    );
  }
}

CoolThing.propTypes = {
  propBoolean : PropTypes.bool.isRequired,
  propNumber  : PropTypes.number,
  propString  : PropTypes.string.isRequired,
};
