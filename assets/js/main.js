// Libs / Helpers
import _assignIn from 'lodash/assignIn';
import TweenMax from 'gsap/TweenMax';

// Relative Libs / Helpers
import { square } from './utilities/math';

import './renderers/CoolThingRenderer.jsx';

console.log('MAIN APP JS', square(12));

// =============================================================================
// EXAMPLE > Lodash
// =============================================================================
function Foo() {
  this.a = 1;
}

function Bar() {
  this.c = 3;
}

Foo.prototype.b = 2;
Bar.prototype.d = 4;

const OBJECT = _assignIn(new Foo(), { a: 0, b: 87 }, new Bar());

console.log('OBJECT', OBJECT);

// =============================================================================
// EXAMPLE > TweenMax
// =============================================================================
const movingThing = document.querySelector('.moving-thing');

const move = () => {
  console.log('PAGE LOADED');
  TweenMax.to(movingThing, 4, { x: 200 });
};

window.addEventListener('load', move);
