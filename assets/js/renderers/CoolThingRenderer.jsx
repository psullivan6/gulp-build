// Libs / Helpers
import React from 'react';
import { render } from 'react-dom';

// Components
import CoolThing from '../components/CoolThing.jsx';

const target = document.getElementById('app-root');

render(<CoolThing propNumber={ 123123 } propBoolean={ true } propString="asfdasdf" />, target);
