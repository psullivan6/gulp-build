# Asset Assumptions

**No move tasks, go directly to `/public`**  
Any source asset that does not need transformation prior to use on production
can be placed directly in the built public directory. Once added to the public
directory, a corresponding glob should be added to the .gitignore, ensuring the
files remain in source control as well as after a scripted cleaning process.
Examples:  
- `data` / `.json`
- `fonts`
- `audio` <-- should be CDN'd
- `video` <-- should be CDN'd















# Styles

## Issues

### Efficient Authoring    
CSS can be written efficiectly, modularly, and easily.

- SCSS: provides a framework for modular imports


### Correct order    
high-level styles are included in the final CSS file above more specific styles


### DRYness    
duplicate style code is not repeated in the final CSS file

- ~~Minification~~: removes duplicate rule-sets, but DOES NOT preserve explicit order (gulp-cssnano uses [postcss-discard-duplicates](https://github.com/ben-eb/postcss-discard-duplicates) <sup>[1](#discard-duplicates-footnote)</sup>)
- ~~Import Once~~: removes duplicate rule-sets AND preserves explicit ordering, but DOES NOT allow for clean npm import statements
- ~~Node Sass Import~~: clean npm import statements, but DOES NOT preserve explicit order




Node Sass Import:
can do `@import "utilities/variables"` with an `_index.scss` file that contains the directory contents
OR
can do `@import "utilities/variables/*"`, which will wildcard all the directory contents

Issue with 
https://github.com/ben-eb/postcss-discard-duplicates/issues/42


<a name="discard-duplicates-footnote">1</a>: https://github.com/ben-eb/postcss-discard-duplicates/issues/42


# TEST - expected order:

1. Local - global
1. NPM   - slope-calc
1. Local - HoverCard
1. Local - CardGrid
1. Local - *variables (color, layout)*
1. Local - Button
1. Local - home
1. Local - products




## Solutions

node-sass-import-once
node-sass
node-sass-import
sass-module-importer


gulp-sass USES -> node-sass








**Browser Prefixes:** correct cross-browser prefixes ONLY to specific declarations

## Solutions

**Minification:** DRY, but unperscribed order, so specificity incorrect
**Autoprefixer:**