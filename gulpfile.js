const gulp = require('gulp');

const tasks = require('./gulp');

const build = gulp.parallel(tasks.buildTasks);
const watch = gulp.parallel(tasks.watchTasks);
const clean = require('./gulp/tasks/clean');

gulp.task('build', build);
gulp.task('default', watch);
gulp.task('watch', watch);
gulp.task('clean', clean);
